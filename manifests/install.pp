# Install Bamboo server
class bamboo::install {
  ensure_packages('wget')

  group { $bamboo::group:
    ensure => present,
    gid    => $bamboo::gid,
  }
  -> user { $bamboo::user:
    ensure     => present,
    uid        => $bamboo::uid,
    gid        => $bamboo::group,
    shell      => '/usr/sbin/nologin',
    system     => true,
    managehome => true,
  }
  -> file { [$bamboo::install_dir, $bamboo::data_dir]:
    ensure => directory,
  }
  -> archive { 'atlassian-bamboo.tar.gz':
    path          => "/tmp/bamboo-${bamboo::version}.tar.gz",
    extract       => true,
    extract_path  => $bamboo::install_dir,
    extract_flags => '--strip 1 -xzf',
    source        => $bamboo::archive_url,
    creates       => "${bamboo::install_dir}/bamboo.sh",
    user          => $bamboo::user,
    group         => $bamboo::group,
    provider      => wget,
    require       => [Package['wget']],
  }

  #TODO: Create dependence on Java
}
