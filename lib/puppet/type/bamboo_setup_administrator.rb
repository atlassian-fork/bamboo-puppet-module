require 'puppet/property/boolean'

Puppet::Type.newtype(:bamboo_setup_administrator) do
  @doc = 'Sets up Bamboo server\'s administrator through the setup wizard.'
  ensurable

  newparam(:name) do
    desc 'Name of the setup (i.e. current).'
  end

  newparam(:username) do
    desc ''
  end

  newparam(:password) do
    desc ''
  end

  newparam(:full_name) do
    desc ''
  end

  newparam(:email) do
    desc ''
  end

  autorequire(:bamboo_setup_license) do
      self[:name]
  end

  autorequire(:bamboo_setup_config) do
      self[:name]
  end

  autorequire(:bamboo_setup_database) do
      self[:name]
  end
end
